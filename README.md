# No pls meme bot

Removes `pls meme` messages from discord chats

# Auto deploy
1. Google jib: https://gitlab.com/ahellwig/noplsmemebot/-/blob/master/.gitlab-ci.yml#L26
2. [docker-compose](docker-compose.yml) + `axela.env` file defining `DISCORD_API_KEY=...`
3. Use https://gitlab.com/ahellwig/compose-auto-updater to auto update