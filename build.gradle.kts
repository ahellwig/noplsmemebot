plugins {
  id("java")
  application
  id("com.google.cloud.tools.jib") version "3.0.0"
}

repositories {
  mavenCentral()
  maven {
    url = uri("https://m2.dv8tion.net/releases")
    name = "dv8tion"
  }
}

dependencies {
  implementation("commons-io:commons-io:2.6")
  implementation("net.dv8tion:JDA:4.3.0_331")
  implementation("org.slf4j:slf4j-simple:1.7.25")

  implementation("org.hibernate:hibernate-core:5.4.32.Final")
  implementation("org.xerial:sqlite-jdbc:3.34.0")
}

application {
  mainClassName = "de.ahellwig.Main"
}

java {
  sourceCompatibility = JavaVersion.VERSION_1_8
  targetCompatibility = JavaVersion.VERSION_1_8
}

if(project.findProperty("jib.active") != null) {
  jib {
    from {
      image = "gcr.io/distroless/java:11"
    }
    to {
      image = System.getenv("CI_REGISTRY_IMAGE") as String
      auth {
        username = System.getenv("CI_REGISTRY_USER") as String
        password = System.getenv("CI_REGISTRY_PASSWORD") as String
      }
    }
  }
}