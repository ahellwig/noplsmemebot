package de.ahellwig;

import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.JDABuilder;
import net.dv8tion.jda.api.entities.*;
import net.dv8tion.jda.api.events.interaction.ButtonClickEvent;
import net.dv8tion.jda.api.events.interaction.SlashCommandEvent;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import net.dv8tion.jda.api.events.user.UserActivityStartEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;
import net.dv8tion.jda.api.interactions.components.ActionRow;
import net.dv8tion.jda.api.interactions.components.Button;
import net.dv8tion.jda.api.requests.GatewayIntent;
import net.dv8tion.jda.api.utils.MemberCachePolicy;
import net.dv8tion.jda.api.utils.cache.CacheFlag;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.jetbrains.annotations.NotNull;

import javax.annotation.Nonnull;
import javax.security.auth.login.LoginException;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.*;

public class Main {
    private static final String ENCODING = "UTF-8";
    private static List<String> games = new ArrayList<>();
    private static List<String> drops = new ArrayList<>();
    private static List<String> version = new ArrayList<>();
    private static List<String> deleteMessages = new ArrayList<>();
    private static JDA jda;
    private static int lastDValue = 0;
    private static Random random = new Random();
    private static String nonce = "" + Math.abs(random.nextInt());
    private static boolean muteActive = false;

    public static void main(String[] args) throws IOException, LoginException {
        //System.setProperty(org.slf4j.impl.SimpleLogger.DEFAULT_LOG_LEVEL_KEY, "TRACE");

        JDABuilder jdaBuilder = JDABuilder.createDefault(getApiKey());
        jdaBuilder.addEventListeners(new ListenerAdapter() {
            @Override
            public void onMessageReceived(@NotNull MessageReceivedEvent event) {
                handleMute(event);
                if(!muteActive) {
                    handlePlsMeme(event);
                    handleGame(event);
                    handleDrop(event);
                    handleVersion(event);
                    handleInfo(event);
                    handleChoose(event);
                    handleDebug(event);
                }
            }

            @Override
            public void onSlashCommand(@NotNull SlashCommandEvent event){
                if(event.getName().equals("wuerfeln")){
                    event.deferReply().setEphemeral(true).queue();
                    event.getHook().sendMessage("Bitte Würfel auswählen:")
                            .addActionRow(
                                    Button.primary("d4", "D4"),
                                    Button.primary("d6", "D6"),
                                    Button.primary("d10", "D10"),
                                    Button.primary("d12", "D12"),
                                    Button.primary("d20", "D20")
                            ).setEphemeral(true)
                            .queue();
                }
            }

            @Override
            public void onUserActivityStart(@Nonnull UserActivityStartEvent event) {
                GuildVoiceState voiceState = event.getMember().getVoiceState();
                if (voiceState != null && voiceState.inVoiceChannel()) {
                    handleStatusDrop(Collections.singletonList(event.getNewActivity()));
                }
            }


            @Override
            public void onButtonClick(@NotNull ButtonClickEvent event) {
                try {
                    String id = event.getButton().getId();
                    if(id.startsWith("d")){
                        lastDValue = Integer.parseInt(id.substring(1));

                        event.reply("Wie viele?")
                                .addActionRow(
                                        Button.primary("w1", "1"),
                                        Button.primary("w2", "2"),
                                        Button.primary("w3", "3"),
                                        Button.primary("w4", "4"),
                                        Button.primary("w5", "5")
                                )
                                .addActionRow(
                                        Button.primary("w6", "6"),
                                        Button.primary("w7", "7"),
                                        Button.primary("w8", "8"),
                                        Button.primary("w9", "9"),
                                        Button.primary("w10", "10")
                                )
                                .queue();
                    }

                    if(id.startsWith("w")){
                        Random random = new Random();
                        int num = Integer.parseInt(id.substring(1));
                        String msg = num + " x D" + lastDValue + "\n";
                        for(int i = 1; i <= num; i++){
                            msg += i + ". " + (random.nextInt(lastDValue) + 1) + "\n";
                        }

                        event.reply(msg).queue();
                    }
                }catch(Exception exception){

                }
            }
        });

        jda = jdaBuilder
                .enableIntents(GatewayIntent.GUILD_PRESENCES)
                .setMemberCachePolicy(MemberCachePolicy.ONLINE)
                .enableCache(EnumSet.of(CacheFlag.ACTIVITY))
                .build();

        //jda.updateCommands().addCommands(new CommandData("wuerfeln", "Buttons zum wuerfeln")).queue();

        games = loadLinesFromResource("games.txt");
        drops = loadLinesFromResource("drops.txt");
        version = loadLinesFromResource("version.txt");
        deleteMessages = loadLinesFromResource("deleteMessages.txt");

        final File debugUsersFile = new File("debugUsers.txt");

        for (String userIdStr : FileUtils.readLines(debugUsersFile, StandardCharsets.UTF_8)) {
            try{
                long uid = Long.parseLong(userIdStr.trim());
                User user = jda.getUserById(uid);
                if(user != null){
                    debugIdToUser.put(uid, user);
                }
            }catch (Exception e){
                // Ignore
            }
        }

        Runtime.getRuntime().addShutdownHook(new Thread(() -> {
            try {
                FileUtils.writeLines(debugUsersFile, debugIdToUser.keySet());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }));
    }

    private static void handleMute(MessageReceivedEvent event) {
        String content = getContent(event);
        if(content.matches("!mute \\d+")){
            if(content.substring("!mute ".length()).equals(nonce)){
                muteActive = !muteActive;
                event.getChannel().sendMessage("Muted: " + muteActive).queue();
            }
        }

    }

    private static final Map<Long, User> debugIdToUser = new HashMap<>();

    private static void handleDebug(MessageReceivedEvent event) {
        String content = getContent(event);
        if (content.startsWith("!debug"))
            if (content.contains("true")) {
                User author = event.getAuthor();
                author.openPrivateChannel().flatMap(privateChannel -> privateChannel.sendMessage("Subscribed to debug info")).queue();
                debugIdToUser.put(author.getIdLong(), author);
            }else if(content.contains("false")){
                User author = event.getAuthor();
                author.openPrivateChannel().flatMap(privateChannel -> privateChannel.sendMessage("Unsubscribed to debug info")).queue();
                debugIdToUser.remove(author.getIdLong());
            }else if(content.contains("activity")){
                if(event.getMember() != null) {
                    event.getChannel().sendMessage(event.getMember().getActivities().toString()).queue();
                }
            }
    }

    private static MWStatus mwStatus = MWStatus.NONE;
    private static long lastDrop = 0L;

    private static void handleStatusDrop(List<Activity> activities) {
        for (Activity a : activities) {
            if (a instanceof RichPresence) {
                RichPresence rp = (RichPresence) a;
                if (a.getName().contains("Modern Warfare") && rp.getDetails() != null) {
                    if (rp.getDetails().contains("In pre-game lobby")) {
                        if (!mwStatus.equals(MWStatus.PRE_GAME_LOBBY)) {
                            long now = System.currentTimeMillis();
                            if (now - lastDrop > 2 * 60 * 1000L) {
                                lastDrop = now;
                                String drop = getRandomFromList(drops);

                                for (TextChannel textChannel : jda.getTextChannelsByName("bot-dev", true)) {
                                    textChannel.sendMessage(drop).queue();
                                }

                                for (User user : debugIdToUser.values()) {
                                    user.openPrivateChannel().flatMap(pc -> pc.sendMessage(drop)).queue();
                                }
                            }
                            mwStatus = MWStatus.PRE_GAME_LOBBY;
                        }
                    }

                    if (rp.getDetails().toLowerCase().contains("party")) {
                        mwStatus = MWStatus.PARTY;
                    }
                }
            }
        }


    }

    private static void handleGame(MessageReceivedEvent event) {
        if (getContent(event).startsWith("!game")) {
            event.getChannel()
                    .sendMessage(getRandomFromList(games))
                    .queue();
        }
    }

    private static void handlePlsMeme(MessageReceivedEvent event) {
        Message message = event.getMessage();
        String content = getContent(event);
        if (deleteMessages.stream().anyMatch(content::startsWith)) {
            message.delete().queue();
        }
    }

    private static void handleDrop(MessageReceivedEvent event) {
        if (getContent(event).startsWith("!drop")) {
            event.getChannel()
                    .sendMessage(getRandomFromList(drops))
                    .queue();
        }
    }

    private static void handleVersion(MessageReceivedEvent event) {
        if (getContent(event).startsWith("!version")) {
            event.getChannel()
                    .sendMessage(String.join("\n", version) + "\n" + nonce)
                    .queue();
        }
    }

    private static void handleInfo(MessageReceivedEvent event) {
        if (getContent(event).startsWith("!git") || getContent(event).startsWith("!info")) {
            event.getChannel()
                    .sendMessage("Git")
                    .setActionRow(ActionRow.of(Button.link("https://gitlab.com/ahellwig/noplsmemebot","gitlab.com/ahellwig/noplsmemebot")).getComponents())
                    .queue();
        }
    }

    private static void handleChoose(MessageReceivedEvent event) {
        String content = getContent(event);
        String prefix = "!choose ";
        if (content.startsWith(prefix)) {
            content = content.substring(prefix.length()).trim();
            List<String> parts = Arrays.asList(content.split("\\s*,\\s*"));
            event.getChannel()
                    .sendMessage(getRandomFromList(parts))
                    .queue();
        }


    }

    private static String getContent(MessageReceivedEvent event) {
        return event.getMessage().getContentDisplay().trim().toLowerCase();
    }

    private static String getRandomFromList(List<String> list) {
        int index = new Random().nextInt(list.size());
        return list.get(index);
    }

    private static String getApiKey() throws IOException {
        String discord_api_key = "DISCORD_API_KEY";
        if (System.getenv().containsKey(discord_api_key)) {
            return System.getenv(discord_api_key);
        }

        return String.join("", loadLinesFromResource("secrets/discordKey.txt"));
    }

    private static List<String> loadLinesFromResource(String filename) throws IOException {
        ClassLoader classLoader = Main.class.getClassLoader();
        URL resource = classLoader.getResource(filename);
        Objects.requireNonNull(resource, "Resource " + filename + " not found");
        return IOUtils.readLines(resource.openStream(), ENCODING);
    }
}
